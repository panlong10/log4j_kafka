package com.ecochain.kafka_log4j;

 
import org.apache.log4j.Logger;

 
/**
 * Unit test for simple App.
 */
public class AppTest 
     
{
    private static final Logger LOGGER = Logger.getLogger(AppTest.class);
    public static void main(String[] args) throws InterruptedException {
    	//log record into kafka
        for (int i = 0; i < 20; i++) {
            LOGGER.info("Info [" + i + "]");
            Thread.sleep(1000);
        }
    }
}
